<?php

use App\Post;
use App\Region;
use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $regions = Region::all();
        $regions->each(function(Region $region) {
            factory(Post::class, random_int(3,7))->create(['category_id' => $region->id]);
        });
    }
}
