<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

class Region extends Model
{
    use NodeTrait;

    public function posts()

    {

        return $this->hasMany(Post::class, 'category_id');

    }
//    public function parent()
//    {
//
//        return $this->belongsTo(static::class, 'parent_id', 'id');
//
//    }
//
//    public function children()
//    {
//
//        return $this->hasMany(static::class, 'parent_id', 'id');
//
//    }
}
