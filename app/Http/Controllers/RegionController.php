<?php

namespace App\Http\Controllers;

use App\Post;
use App\Region;
use Illuminate\Http\Request;

class RegionController extends Controller
{
    public function index()
    {
        $regions = Region::defaultOrder()->withDepth()->get()->toFlatTree();

        $region = Region::find(1);
        // Get ids of descendants
        $categories = $region->descendants()->pluck('id');

// Include the id of category itself
        $categories[] = $region->getKey();

// Get goods
        $goods = Post::whereIn('category_id', $categories)->get();
        dd($goods);

        $traverse = function ($categories) use (&$traverse) {
            echo '<ul>';
            foreach ($categories as $category) {
                echo '<li>'.$category->name.'</li>';
                $traverse($category->children);
            }
            echo '</ul>';
        };

        return view('admin.regions.index')->with([
            'regions' => $regions,
            'traverse' => $traverse
        ]);
    }
}
